<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_submit</name>
   <tag></tag>
   <elementGuidId>cde84f87-fa9d-4227-81a6-176db3c6e3c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#file-submit</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='file-submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2bc8b230-1916-4fee-819e-2917f2d52a08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e22c3355-f676-433e-b4db-ab2d83e2ff29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file-submit</value>
      <webElementGuid>c7e0dca0-05d5-421d-ab6b-72b9a2a648fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>06ff5620-6a3b-4f62-84ca-232a7905a871</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Upload</value>
      <webElementGuid>765354b9-aab6-4ab8-90ed-6a8a567b858f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;file-submit&quot;)</value>
      <webElementGuid>46cea810-5143-4eea-be8c-951ff1d70315</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='file-submit']</value>
      <webElementGuid>d363b763-ec0a-498b-9725-778d7cf197e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/form/input[2]</value>
      <webElementGuid>96c7a7c3-a179-4049-bbdc-008817d259c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input[2]</value>
      <webElementGuid>fd42fe24-1e74-4e22-9659-dbf85835f7c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'file-submit' and @type = 'submit']</value>
      <webElementGuid>d4dbb5a9-f15b-4b67-9973-805189cafff1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
