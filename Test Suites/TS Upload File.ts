<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Upload File</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e90a0a5b-a69e-4a38-b2e7-965986a3ed8b</testSuiteGuid>
   <testCaseLink>
      <guid>6fd744b4-1996-4b25-bd5c-3f8c00c1c5ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC Upload File Data Driven Method</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c282fba1-f3fc-4a3a-b94e-96729d820bc7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Upload</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>c282fba1-f3fc-4a3a-b94e-96729d820bc7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>fileName</value>
         <variableId>f0b86e1a-29ad-4f4a-8b96-767dc1f1a395</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
